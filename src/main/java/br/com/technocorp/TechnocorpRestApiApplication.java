package br.com.technocorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("dataPoa.dataPoaRestAPI.models")
@SpringBootApplication
public class TechnocorpRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnocorpRestApiApplication.class, args);
	}
}
