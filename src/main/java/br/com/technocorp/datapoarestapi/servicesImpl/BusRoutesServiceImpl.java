package br.com.technocorp.datapoarestapi.servicesImpl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.technocorp.datapoarestapi.models.BusRoute;
import br.com.technocorp.datapoarestapi.repositories.BusRoutesRepository;
import br.com.technocorp.datapoarestapi.services.BusRoutesService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class BusRoutesServiceImpl implements BusRoutesService {

	@Value("url_itinerarios")
    private String BUS_LINE_URL;

	private WebClient client = WebClient.create(BUS_LINE_URL);

    private BusRoutesRepository repository;


    public Flux<BusRoute> getAllLinhasOnibus() {
    	return client.get()
                .uri("php/facades/process.php?a=il&p=", "", "")
               .retrieve()
               .bodyToFlux(BusRoute.class);
    }

    public void save(BusRoute busRoute) {
    	repository.save(busRoute);
    }

    public void saveRoutesFromService() throws IOException {
        repository.saveAll(this.getAllLinhasOnibus().toStream().collect(Collectors.toList()));
    }

    @Override
    public List<BusRoute> findByNome(String nome) {
        return repository.findByName(nome);
    }

    @Override
    public boolean existsByNameOrCode(String name, String code) {
        return repository.existsByNameOrCode(name, code);
    }

    public List<BusRoute> findAll() {
        return repository.findAll();
    }
}
