package br.com.technocorp.datapoarestapi.servicesImpl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.technocorp.datapoarestapi.models.Itinerary;
import br.com.technocorp.datapoarestapi.repositories.BusRoutesRepository;
import br.com.technocorp.datapoarestapi.repositories.ItineraryRepository;
import br.com.technocorp.datapoarestapi.services.ItineraryService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class ItineraryServiceImpl implements ItineraryService {

	@Value("url_Itinerarys")
	private static String URL_ITINERARYS;

	WebClient webClient = WebClient.create(URL_ITINERARYS);

	private final ItineraryRepository itineratyRepository;

	private final BusRoutesRepository busRouteRepository;

	@Override
	public Flux<Itinerary> getAllItinerariesByRouteId(String idLinha) throws IOException, JSONException {
		return webClient.get().uri(uriBuilder -> uriBuilder.path(idLinha).build()).retrieve()
				.bodyToFlux(Itinerary.class);
	}

	@Override
	public void saveItinerariesFromService(String idLinha) throws IOException, JSONException {
		itineratyRepository.saveAll(this.getAllItinerariesByRouteId(idLinha).toStream().collect(Collectors.toList()));
	}

	public void save(Itinerary Itinerary) {
		itineratyRepository.save(Itinerary);
	}

	@Override
	public List<Itinerary> findByIdRouteId(String idLinha) {
		return itineratyRepository.findByIdLinha(idLinha);
	}

//    @Override
	public boolean existsByIdLinhaAndLatAndLng(String idLinha, String lat, String lng) {
		return itineratyRepository.existsByidLineBusAndLatAndLng(idLinha, lat, lng);
	}

	public boolean existsById(String idLinha) {
		return busRouteRepository.existsById(idLinha);
	}

	@Override
	public List<Itinerary> findAll() {
		return itineratyRepository.findAll();
	}
}
