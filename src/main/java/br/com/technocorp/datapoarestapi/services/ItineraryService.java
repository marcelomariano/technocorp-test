package br.com.technocorp.datapoarestapi.services;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;

import br.com.technocorp.datapoarestapi.models.Itinerary;
import reactor.core.publisher.Flux;

public interface ItineraryService {

    List<Itinerary> findByIdRouteId(String idLinha);

    Flux<Itinerary> getAllItinerariesByRouteId(String idLinha) throws IOException, JSONException;

	List<Itinerary> findAll();

	void saveItinerariesFromService(String idLinha) throws IOException, JSONException;

	boolean existsByIdLinhaAndLatAndLng(String idLinha, String lat, String lng);

	boolean existsById(String idLinha);

	void save(Itinerary Itinerary);

}
