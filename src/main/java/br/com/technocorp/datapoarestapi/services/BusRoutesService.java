package br.com.technocorp.datapoarestapi.services;

import java.util.List;

import br.com.technocorp.datapoarestapi.models.BusRoute;

public interface BusRoutesService {

    List<BusRoute> findByNome(String nome);

    boolean existsByNameOrCode(String nome, String codigo);

    void save(BusRoute busRoute);
}
