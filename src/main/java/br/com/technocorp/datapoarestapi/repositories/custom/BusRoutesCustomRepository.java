package br.com.technocorp.datapoarestapi.repositories.custom;

public interface BusRoutesCustomRepository {

	boolean existsByNameOrCode(String name, String code);

}
