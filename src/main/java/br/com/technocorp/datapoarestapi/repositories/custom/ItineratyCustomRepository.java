package br.com.technocorp.datapoarestapi.repositories.custom;

public interface ItineratyCustomRepository {

	boolean existsByidLineBusAndLatAndLng(String idLinha, String lat, String lng);

}
