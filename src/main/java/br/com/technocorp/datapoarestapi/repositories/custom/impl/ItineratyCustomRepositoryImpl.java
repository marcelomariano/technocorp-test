package br.com.technocorp.datapoarestapi.repositories.custom.impl;

import java.util.Objects;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import br.com.technocorp.datapoarestapi.models.Itinerary;
import br.com.technocorp.datapoarestapi.repositories.custom.ItineratyCustomRepository;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ItineratyCustomRepositoryImpl implements ItineratyCustomRepository {

	private final MongoTemplate mongoTemplate;

	@Override
	public boolean existsByidLineBusAndLatAndLng(String idLineBus, String lat, String lng) {

		return Objects
				.nonNull(mongoTemplate.findOne(
						new Query().addCriteria(Criteria.where("idLineBus").exists(true)
								.andOperator(Criteria.where("lat").is(lat), Criteria.where("lng").is(lng))),
						Itinerary.class));

	}

}
