package br.com.technocorp.datapoarestapi.repositories.custom.impl;

import java.util.Objects;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import br.com.technocorp.datapoarestapi.models.BusRoute;
import br.com.technocorp.datapoarestapi.repositories.custom.BusRoutesCustomRepository;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class BusRoutesCustomRepositoryImpl implements BusRoutesCustomRepository {

	private final MongoTemplate mongoTemplate;

	@Override
	public boolean existsByNameOrCode(String name, String code) {
		return Objects
		.nonNull(mongoTemplate.findOne(
				new Query().addCriteria(Criteria.where("id").exists(true)
						.orOperator(Criteria.where("name").is(name), Criteria.where("code").is(code))),
				BusRoute.class));
	}

}
