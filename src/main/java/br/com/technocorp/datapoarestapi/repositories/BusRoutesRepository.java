package br.com.technocorp.datapoarestapi.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.technocorp.datapoarestapi.models.BusRoute;
import br.com.technocorp.datapoarestapi.repositories.custom.BusRoutesCustomRepository;

@Repository
public interface BusRoutesRepository extends MongoRepository<BusRoute, String>, BusRoutesCustomRepository {

	List<BusRoute> findByName(String name);

	boolean existsById(String id);

}
