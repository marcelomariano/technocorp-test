package br.com.technocorp.datapoarestapi.repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.technocorp.datapoarestapi.models.Itinerary;
import br.com.technocorp.datapoarestapi.repositories.custom.ItineratyCustomRepository;

@Repository
public interface ItineraryRepository extends MongoRepository<Itinerary, String>, ItineratyCustomRepository {

	List<Itinerary> findByIdLinha(String idLinha);

}
