package br.com.technocorp.datapoarestapi.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.technocorp.datapoarestapi.models.BusRoute;
import br.com.technocorp.datapoarestapi.servicesImpl.BusRoutesServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/routes")
@RequiredArgsConstructor
public class BusRoutesController {

    private BusRoutesServiceImpl busRoutesService;

    @GetMapping("/get-from-service")
    public List<BusRoute> getAll() throws IOException {
        return busRoutesService.getAllLinhasOnibus().toStream().collect(Collectors.toList());
    }

    @GetMapping("/name/{name}/**")
    private Flux findByName(String name){
    	return Flux.fromArray(busRoutesService.findByNome(name).toArray());
    }

    @GetMapping({"/", "/list"})
    public Flux findAll() {
    	return Flux.fromArray(busRoutesService.findAll().toArray());
    }

    @PostMapping("/post-from-service")
    public void saveRoutesFromService() throws IOException {
        busRoutesService.saveRoutesFromService();
    }

    @PostMapping("/new")
    @SneakyThrows
    public String save(@RequestBody BusRoute busRoute) {
    	Optional.of(busRoutesService.existsByNameOrCode(busRoute.getName(), busRoute.getCode()))
    	.filter(Boolean::booleanValue).map(m -> {
    		if (m) {
    			return "This Bus Route or Code can't be saved. It is already exists!";
			}
    		busRoutesService.save(busRoute);
    		return "Bus Route save successfully!";
    	});
    	return null;
    }
}
