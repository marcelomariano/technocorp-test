package br.com.technocorp.datapoarestapi.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.json.JSONException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.technocorp.datapoarestapi.models.Itinerary;
import br.com.technocorp.datapoarestapi.services.ItineraryService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
@RequestMapping("/itineraries")
public class ItineraryController {

	private final ItineraryService service;

	@GetMapping("/get-from-service/{route}")
	public Flux getItineraryByRouteId(@PathVariable("routeId") String routeId)
			throws IOException, JSONException {
		return service.getAllItinerariesByRouteId(routeId);
	}

	@GetMapping("/route/{routeId}")
	public Flux findByRouteId(@PathVariable("routeId") String routeId) {
		return Flux.fromArray(service.findByIdRouteId(routeId).toArray());
	}

	@GetMapping("/list")
	public Flux findAll() {
		return Flux.fromArray(service.findAll().toArray());
	}

	@PostMapping("/post-from-service/{routeId}")
	public void saveItinerarysFromService(@PathVariable String routeId) throws IOException, JSONException {
		service.saveItinerariesFromService(routeId);
	}

	@PostMapping("/new")
	@SneakyThrows
	public String save(@RequestBody Itinerary itinerary) {
		Optional.of(service.existsById(itinerary.getIdLineBus())).filter(Boolean::booleanValue).map(m -> {
			return "Bus Route not found";
		});
		Optional.of(
				service.existsByIdLinhaAndLatAndLng(itinerary.getIdLineBus(), (String)itinerary.getCoordenates().get("lat"), (String)itinerary.getCoordenates().get("lng")))
				.filter(Boolean::booleanValue).map(m -> {
					if (m) {
						return "There is a Bus Route registered for this Itinerary!";
					}
					service.save(itinerary);
					return "Itinerary save successfully!";
				});
		return null;
	}
}
