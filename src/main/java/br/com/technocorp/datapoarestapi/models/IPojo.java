package br.com.technocorp.datapoarestapi.models;

import java.io.Serializable;

public interface IPojo<T> extends Serializable {

	T getId();

}