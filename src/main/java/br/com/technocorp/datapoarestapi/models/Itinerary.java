package br.com.technocorp.datapoarestapi.models;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "itineraries")
public class Itinerary implements IPojo<String> {

	private static final long serialVersionUID = -8886069587197940935L;

	@Id
    private String id;
    @JsonProperty("idLinha")
    private String idLineBus;
    @JsonProperty("nome")
    private String name;
    @JsonProperty("codigo")
    private String code;

    private Map<String, Object> coordenates = new LinkedHashMap<>();

    @JsonAnySetter
    void setCoordenates(String key, Object value) {
        coordenates.put(key, value);
    }

}