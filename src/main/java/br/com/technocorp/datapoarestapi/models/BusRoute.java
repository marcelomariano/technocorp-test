package br.com.technocorp.datapoarestapi.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "busroutes")
public class BusRoute implements IPojo<String> {

	private static final long serialVersionUID = 2003639598492717863L;

	@Id
    private String id;
    @JsonProperty("id")

    private String idLinha;
    @JsonProperty("nome")
    private String name;

    @JsonProperty("codigo")
    private String code;


}