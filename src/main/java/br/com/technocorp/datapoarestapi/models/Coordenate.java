package br.com.technocorp.datapoarestapi.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(collection = "coordenates")
public class Coordenate implements IPojo<String>{

	private static final long serialVersionUID = -9089665708931162730L;

	@Id
    private String id;

    @JsonProperty("lat")
    private String latitude;

    @JsonProperty("lng")
    private String longitude;


}